build/symbolic_regression: build $(wildcard src/**) $(wildcard examples/symbolic_regression/*.cpp)
	g++ -O2 --std=c++2a -pthread -o build/symbolic_regression -rdynamic -g examples/symbolic_regression/main.cpp -lstdc++ -lm -ldl -lbfd

build/random: build $(wildcard src/**) $(wildcard examples/random_test/*.cpp)
	gcc -O2 --std=c++2a -o build/random -lstdc++ -lm examples/random_test/main.cpp

build:
	mkdir build
