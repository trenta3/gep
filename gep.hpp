#ifndef GEP
#define GEP
#include "src/utils.hpp"
#include "src/terminals.hpp"
#include "src/parallel.hpp"
#include "src/selection.hpp"
#include "src/evolution.hpp"
#include "src/configuration.hpp"
#include "src/random_selection.hpp"
#include "src/gene.hpp"
#include "src/chromosome.hpp"
#include "src/functions.hpp"
#include "src/linking.hpp"
#include "src/population.hpp"
#include "src/selection_roulettewheel.hpp"

#define BACKWARD_HAS_BFD 1
#include "backward.hpp"
namespace backward {
  backward::SignalHandling sh;
}
#endif
