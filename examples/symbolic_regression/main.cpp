#include "../../gep.hpp"
#include <iostream>
#include <array>
#include <vector>
#include <ctime>

using namespace std;

#define TEST_SIZE 10
#define POLYVAL(x) (4 * x * x * x + 2 * x + 5)

static inline double get_x (double x) { return x; }
constexpr Terminal<double, double> x = make_terminal<double, double, get_x>("x");

class TestData {
public:
  double x;
  double y;

  inline TestData(double x) {
    this->x = x;
    this->y = POLYVAL(x);
  }
};

vector<TestData> tests;

template <typename T, typename O>
class CustomChromosome : public Chromosome<T, O> {
public:
  inline CustomChromosome (Configuration<T, O> *configuration)
    : Chromosome<T, O>(configuration) {}

  inline double _fitness () override {
    double fit = 0;
    for (TestData p: tests) {
      double guess = (*this)(p.x);
      double diff = min(1.0, abs((p.y - guess) / p.y));
      fit += 1000.0 * (1 - diff);
    }
    return fit;
  }

  inline double _solved () override {
    return this->fitness() == TEST_SIZE * 1000.0;
  }
};

int main () {
  vector<Terminal<double, double>> function_terms = vector<Terminal<double, double>>({addt, mult, subt, divt});
  vector<Terminal<double, double>> terminal_terms = vector<Terminal<double, double>>({x});
  linking_fptr<double> linking_function = addition_linker;
  EvolutionRates erates = EvolutionRates(2.5/45, 0.1, 0);
  Configuration<double, double> configuration = Configuration<double, double> (6, 3, function_terms, terminal_terms, linking_function, erates);
  RouletteWheelSelection<double, double, CustomChromosome<double, double>> selection_strategy = RouletteWheelSelection<double, double, CustomChromosome<double, double>>();
  Population<double, double, CustomChromosome<double, double>> population = Population<double, double, CustomChromosome<double, double>>(30, &configuration, &selection_strategy);

  standard_random<> random {};

  cout << "Populating test points" << endl;
  for (uint i = 0; i < TEST_SIZE; i++)
    tests.push_back(TestData(10 * random()));

  for (auto p: tests)
    cout << "(" << p.x << ", " << p.y << ") ";
  
  cout << endl << "Population initialized, stating computation..." << endl;
  cout << population << endl;

  clock_t start = clock();
  for (uint i = 0; i < 2000; i++) {
    population.cycle();
    if (i % 50 == 0)
      cout << i << ": " << population.best() << " " << population.best().fitness() << " [" << population.best().len() << "]" << endl;

    if (population.best()._solved())
      break;
  }
  clock_t end = clock();
  double microsec_percycle = 1000.0 * (end - start) / (population.cyclenum * CLOCKS_PER_SEC);
  cout << "Final solution: " << population.best() << " " << population.best().fitness() << " [" << population.best().len() << "]" << endl;
  printf("Time per cycle: %.3fms\n", microsec_percycle);
  return 0;
}
