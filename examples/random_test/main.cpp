#include "../../gep.hpp"

#define TEST_SAMPLE 1000

uint counte (double *distr, uint idx) {
  uint count = 0;
  for (uint i = 0; i < TEST_SAMPLE; i++)
    if ((distr[i] >= idx * 0.1) && (distr[i] < (idx + 1) * 0.1))
      count++;
  return count;
}

int main () {
  standard_random<> random {};
  double distribution[TEST_SAMPLE];

  for (uint i = 0; i < TEST_SAMPLE; i++)
     distribution[i] = random();

  for (uint j = 0; j < 10; j++)
    printf("%.1f - %.1f | %d\n", j * 0.1, (j+1) * 0.1, counte((double *) distribution, j));

  return 0;
}
