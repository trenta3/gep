#ifndef GEP_SELECTION_ROULETTEWHEEL
#define GEP_SELECTION_ROULETTEWHEEL
#include "selection.hpp"
#include "population.hpp"
#include <random>
#include <algorithm>

using namespace std;

inline uint insertion_place (double *array, uint size, double tosearch) {
  for (uint place = 0; place < size; place++)
    if (array[place] >= tosearch)
      return place;
  return size - 1;
}

template <typename T, typename O, class DerivedChromosome>
class RouletteWheelSelection : public SelectionStrategy<T, O, DerivedChromosome> {
public:
  mt19937 randeng;

  inline RouletteWheelSelection () {
    random_device randev;
    this->randeng = mt19937(randev());
  }
  
  inline vector<DerivedChromosome> select (Population<T, O, DerivedChromosome> *population) override {
    vector<DerivedChromosome> newp;
    uint toselect = population->population_size - 1;
    double cumulative_fitness[population->population_size];

    newp.push_back(population->best());
    cumulative_fitness[0] = population->population[0].fitness();
    for (uint i = 1; i < population->population_size; i++)
      cumulative_fitness[i] = cumulative_fitness[i-1] + population->population[i].fitness();

    uniform_real_distribution<double> distrib(0.0, cumulative_fitness[toselect]);

    for (; toselect > 0; toselect--) {
      uint index = insertion_place(cumulative_fitness, population->population_size, distrib(this->randeng));
      newp.push_back(population->population[index]);
    }
    return newp;
  }
};
#endif
