#ifndef GEP_RANDOM_SELECTION
#define GEP_RANDOM_SELECTION
#include <random>
#include <iterator>

using namespace std;

template <typename RandomGenerator = default_random_engine>
struct random_selector {
  RandomGenerator gen;

public:
  random_selector () {
    this->gen = RandomGenerator(random_device()());
  }

  template <typename Iter>
  Iter select(Iter start, Iter end) {
    uniform_int_distribution<> dis(0, distance(start, end) - 1);
    advance(start, dis(gen));
    return start;
  }

  template <typename Iter>
  Iter operator() (Iter start, Iter end) {
    return select(start, end);
  }

  template <typename Container>
  auto operator() (const Container& c) -> decltype(*begin(c))& {
			     return *select(begin(c), end(c));
  }
};
#endif
