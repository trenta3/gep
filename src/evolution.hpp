#ifndef GEP_EVOLUTION
#define GEP_EVOLUTION
class EvolutionRates {
public:
  double mutation;
  double head_inversion;
  double tail_inversion;

  inline EvolutionRates (double mutation = 0.1,
			 double head_inversion = 0.1,
			 double tail_inversion = 0.05) :
    mutation(mutation),
    head_inversion(head_inversion),
    tail_inversion(tail_inversion)
  {}
};
#endif
