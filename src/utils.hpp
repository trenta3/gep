#ifndef GEP_UTILS
#define GEP_UTILS
#include <vector>
#include <exception>
#include <iostream>

using namespace std;

typedef unsigned int uint;
typedef long unsigned int ulint;

template <typename T, typename O>
using termptr = T(*)(vector<T>, O);

template <typename T>
inline ostream& print_with_separator (const vector<T>& vecT, ostream& os, const char *sep) {
  bool first = true;
  for (T el : vecT) {
    if (first) first = false;
    else os << sep;
    os << el;
  }
  return os;
}

struct GEPException : public exception {
public:
  const char *_errormsg;

  GEPException (const char *errormsg) : _errormsg(errormsg) {}

  const char *what() const throw () {
    return this->_errormsg;
  }
};

/*
 *  All functions are to be expressed by the template vector<T> --> T
 */
template <typename T, typename O>
using attribute_fptr = T (*)(O);

template <typename T>
using function_fptr = T (*)(vector<T>);

template <typename T>
using linking_fptr = T (*)(T*, uint);

#define repeat(var, times) \
  for (int var = 0; var < (times); var++)

#endif
