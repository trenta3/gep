#ifndef GEP_TERMINALS
#define GEP_TERMINALS
#include <vector>
#include <string>
#include "utils.hpp"

using namespace std;

template <typename T, typename O, attribute_fptr<T, O> F>
struct terminalAttribute {
  static constexpr T function(vector<T> vecT, O obj) {
    return F(obj);
  }
};

template <typename T, typename O, function_fptr<T> F>
struct terminalFunction {
  static constexpr T function (vector<T> vecT, O obj) {
    return F(vecT);
  }
};

template <typename T, typename O>
class Terminal {
public:
  uint arity;
  const char *name;
  termptr<T, O> function;

  inline Terminal () : arity(0), name(NULL), function(NULL) {}
  
  constexpr inline Terminal (uint arity, const char *name, termptr<T, O> ptr) : arity(arity), name(name), function(ptr) {}

  inline friend ostream& operator<< (ostream& os, const Terminal<T, O>& t) {
    os << t.name;
    return os;
  }
};

template <typename T, typename O, function_fptr<T> F, uint arity>
constexpr Terminal<T, O> make_terminal (const char *name) {
  return Terminal<T, O>(arity, name, terminalFunction<T, O, F>::function);
}

template <typename T, typename O, attribute_fptr<T, O> F>
constexpr Terminal<T, O> make_terminal (const char *name) {
  return Terminal<T, O>(0, name, terminalAttribute<T, O, F>::function);
}
#endif
