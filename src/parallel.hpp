#ifndef GEP_PARALLEL
#define GEP_PARALLEL
#include <algorithm>
#include <thread>
#include <functional>
#include <vector>
#include <iostream>
#include "ctpl_stl.h"

/// @param[in] nb_elements : size of your for loop
/// @param[in] functor(start, end) :
/// your function processing a sub chunk of the for loop.
/// "start" is the first index to process (included) until the index "end"
/// (excluded)
/// @code
///     for(int i = start; i < end; ++i)
///         computation(i);
/// @endcode
/// @param use_threads : enable / disable threads.
///
///
unsigned nb_threads_hint = std::thread::hardware_concurrency();
unsigned nb_threads = nb_threads_hint == 0 ? 4 : nb_threads_hint;
ctpl::thread_pool thrp(nb_threads);

inline void parallel_for(unsigned nb_elements,
			 std::function<void (int tid, int start, int end)> functor,
			 bool use_threads = true)
{
  unsigned batch_size = nb_elements / nb_threads;
  unsigned batch_remainder = nb_elements % nb_threads;

  std::future<void> qw[nb_threads];

  if (use_threads) {
    // Multithread execution
    for (unsigned i = 0; i < nb_threads; ++i) {
      int start = i * batch_size;
      qw[i] = thrp.push(functor, start, start+batch_size);
    }
  } else {
    // Single thread execution (for easy debugging)
    for (unsigned i = 0; i < nb_threads; ++i) {
      int start = i * batch_size;
      functor(0, start, start+batch_size);
    }
  }
  
  // Deform the elements left
  int start = nb_threads * batch_size;
  functor(0, start, start+batch_remainder);
  
  // Wait for the other thread to finish their task
  if (use_threads)
    for (unsigned i = 0; i < nb_threads; i++)
      qw[i].get();
}

#ifdef NOPARALLEL
#warning "Compiling in sequential mode"
#define PARALLEL_FOR_BEGIN(nb_elements, var)	\
  for (int var = 0; var < nb_elements; ++var)
#define PARALLEL_FOR_END
#else
#define PARALLEL_FOR_BEGIN(nb_elements, var) \
  parallel_for(nb_elements,		     \
    [&](int tid, int start, int end) {	     \
    for (int var = start; var < end; ++var)
#define PARALLEL_FOR_END })
#endif

#endif
