#ifndef GEP_LINKING
#define GEP_LINKING
#include <vector>

using namespace std;

inline double addition_linker (double* nums, uint size) {
  double total = 0;
  for (uint i = 0; i < size; i++)
    total += nums[i];
  return total;
}

inline double multiplication_linker (double* nums, uint size) {
  double total = 1;
  for (uint i = 0; i < size; i++)
    total *= nums[i];
  return total;
}

inline double max_linker (double* nums, uint size) {
  double max = nums[0];
  for (uint i = 0; i < size; i++)
    if (nums[i] > max)
      max = nums[i];
  return max;
}

inline double min_linker (double* nums, uint size) {
  double min = nums[0];
  for (uint i = 0; i < size; i++)
    if (nums[i] < min)
      min = nums[i];
  return min;
}

inline uint argmax_linker (double* nums, uint size) {
  uint maxplace = 0;
  for (uint i = 0; i < size; i++)
    if (nums[i] > nums[maxplace])
      maxplace = i;
  return maxplace;
}

inline uint argmin_linker (double* nums, uint size) {
  uint minplace = 0;
  for (uint i = 0; i < size; i++)
    if (nums[i] < nums[minplace])
      minplace = i;
  return minplace;
}
#endif
