#ifndef GEP_POPULATION
#define GEP_POPULATION
#include "chromosome.hpp"
#include "configuration.hpp"
#include "selection.hpp"
#include "parallel.hpp"

using namespace std;

template <typename T, typename O, class DerivedChromosome>
class Population {
public:
  uint population_size;
  vector<DerivedChromosome> population;
  Configuration<T, O> *configuration;
  SelectionStrategy<T, O, DerivedChromosome> *selection;
  uint cyclenum;
  
  inline Population (uint population_size, Configuration<T, O> *configuration, SelectionStrategy<T, O, DerivedChromosome> *selection) {
    static_assert(is_base_of<Chromosome<T, O>, DerivedChromosome>::value, "The population chromosome has to be derived from Chromosome");
    vector<DerivedChromosome> population;

    for (uint i = 0; i < population_size; i++)
      population.push_back(DerivedChromosome(configuration));

    this->configuration = configuration;
    this->population = population;
    this->population_size = population_size;
    this->selection = selection;
    this->cyclenum = 0;
  }

  // Write the representation of the population
  inline friend ostream& operator<< (ostream& os, const Population<T, O, DerivedChromosome>& p) {
    for (DerivedChromosome chromosome: p.population)
      os << chromosome << " " << chromosome.fitness() << "  [" << chromosome.len() << "]" << endl;
    return os;
  }

  // Function to execute a cycle of selection and evaluation of the population
  void cycle () {
    this->cyclenum++;
    // Force evaluation of all the chromosomes
    for (auto& chromosome: this->population)
      chromosome.fitness();
    // Select the chromosomes for the new population
    this->population = this->selection->select(this);
    // Order the population based on fitness
    sort(this->population.begin(), this->population.end(), compare_chromosomes<DerivedChromosome>);
    // Let each chromosome mutate on its own
    for (uint i = 1; i < this->population.size(); i++)
      this->population[i].evolve();
  }

  // Function to get the best chromosome
  DerivedChromosome best() {
    DerivedChromosome best = this->population[0];
    for (DerivedChromosome chromosome : this->population)
      if (chromosome.fitness() > best.fitness())
	best = chromosome;
    return best;
  }
};
#endif
