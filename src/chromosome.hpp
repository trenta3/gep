#ifndef GEP_CHROMOSOME
#define GEP_CHROMOSOME
#include <vector>
#include <iostream>
#include <string>
#include "gene.hpp"
#include "random.hpp"
#include "configuration.hpp"

using namespace std;

template <typename T, typename O>
class Chromosome {
public:
  Configuration<T, O> *configuration;
  vector<Gene<T, O>> genes;
  random_selector<> selector {};
  standard_random<> random {};
  double cached_fitness;
  bool fitness_ok;

  inline Chromosome (Configuration<T, O> *configuration) {
    this->configuration = configuration;
    this->fitness_ok = false;
    this->initialize();
  }

  // Initialize randomly all the genes of the chromosome
  inline void initialize () {
    repeat(i, this->configuration->gene_number)
      this->genes.push_back(Gene<T, O>(this->configuration));
  }

  inline void read_from (string description) {
    istringstream sdescription(description);
    string current;
    for (Gene<T, O>& el: this->genes) {
      getline((istream&) sdescription, current, '|');
      el.read_from(current);
    }
  }

  // Write the representation of the current chromosome
  inline friend ostream& operator<< (ostream& os, const Chromosome<T, O>& c) {
    return print_with_separator(c.genes, os, "|");
  }

  inline T operator() (O obj) {
    T results[this->configuration->gene_number];
    for (uint i = 0; i < this->configuration->gene_number; i++)
      results[i] = this->genes[i](obj);
    return this->configuration->linking_function(results, this->configuration->gene_number);
  }

  // Total length of the chromosome description
  inline uint len () {
    uint total = 0;
    for (Gene<T, O> gene: this->genes)
      total += gene.len();
    return total;
  }

  inline double fitness () {
    if (this->fitness_ok)
      return this->cached_fitness;
    this->cached_fitness = this->_fitness();
    this->fitness_ok = true;
    return this->cached_fitness;
  }
  
  inline virtual double _fitness () {
    throw GEPException("The _fitness function in chromosomes has not been defined");
  }
  
  // By default _solved returns false
  inline virtual double _solved () {
    return false;
  }

  inline void mutate () {
    for (Gene<T, O>& gene: this->genes)
      if (gene.mutate())
	this->fitness_ok = false;
  }

  inline void head_inversion () {
    if (this->random() <= this->configuration->erates.head_inversion) {
      uint toinvert = floor(this->configuration->gene_number * this->random());
      this->genes[toinvert].head_inversion();
      this->fitness_ok = false;
    }
  }

  inline void tail_inversion () {
    if (this->random() <= this->configuration->erates.tail_inversion) {
      uint toinvert = floor(this->configuration->gene_number * this->random());
      this->genes[toinvert].tail_inversion();
      this->fitness_ok = false;
    }
  }

  // Evolve the chromosome
  inline void evolve () {
    this->mutate();
    this->head_inversion();
    this->tail_inversion();
  }
};
#endif
