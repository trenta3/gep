#ifndef GEP_GENE
#define GEP_GENE
#include <vector>
#include <iostream>
#include <string>
#include "configuration.hpp"
#include "random.hpp"
#include "random_selection.hpp"

using namespace std;

template <typename T, typename O>
inline T first_element (vector<T> v, O obj) {
  return v[0];
}

template <typename T, typename O>
class Gene {
public:
  Configuration<T, O> *configuration;
  standard_random<> random {};
  random_selector<> selector {};
  vector<Terminal<T, O>> terminals;

  inline Gene () : configuration(NULL) {}
  
  // Initializes randomly the gene
  inline Gene (Configuration<T, O> *configuration) {
    this->configuration = configuration;
    for (uint i = 0; i < configuration->head_length; i++)
      this->terminals.push_back(this->selector(configuration->all_terms));
    for (uint i = configuration->head_length; i < configuration->total_length; i++)
      this->terminals.push_back(this->selector(configuration->attribute_terms));
  }

  inline pair<T, uint> evaluate (O& obj, uint sp = 0, uint collect = 1, T (*apply_fn)(vector<T>, O) = first_element) {
    vector<T> results(collect);
    for (uint i = 0; collect > 0; collect--, i++) {
      Terminal<T, O>& element = this->terminals[sp];
      pair<T, uint> value = this->evaluate(obj, ++sp, element.arity, element.function);
      results[i] = value.first;
      sp = value.second;
    }
    return make_pair(apply_fn(results, obj), sp);
  }
  
  // Stack-based execution machine
  inline T operator() (O obj) {
    return this->evaluate(obj).first;
  }

  // Write the representation of the current gene
  inline friend ostream& operator<< (ostream& os, const Gene<T, O>& g) {
    return print_with_separator(g.terminals, os, ".");
  }

  inline void read_from (string description) {
    istringstream sdescription(description);
    string current;
    uint i = 0;
    while(getline((istream&) sdescription, current, '.')) {
      for (Terminal<T, O>& term: this->configuration->all_terms) {
	if (term.name == current)
	  this->terminals[i] = term;
      }
      i++;
    }
  }
  
  inline uint len (uint sp = 0, uint collect = 1) {
    for (; collect > 0; collect--) {
      uint arity = this->terminals[sp].arity;
      sp = this->len(++sp, arity);
    }
    return sp;
  }

  inline bool mutate () {
    bool modified = false;
    for (uint i = 0; i < this->terminals.size(); i++) {
      if (this->random() <= this->configuration->erates.mutation) {
	if (i < this->configuration->head_length)
	  this->terminals[i] = this->selector(this->configuration->all_terms);
	else
	  this->terminals[i] = this->selector(this->configuration->attribute_terms);
	modified = true;
      }
    }
    return modified;
  }

  inline bool head_inversion () {
    uint initial_head_point = floor(this->random() * this->configuration->head_length);
    uint final_head_point = floor(this->random() * this->configuration->head_length);
    uint start = min(initial_head_point, final_head_point);
    uint end = max(initial_head_point, final_head_point) + 1;
    reverse(this->terminals.begin() + start, this->terminals.begin() + end);
    return true;
  }

  inline bool tail_inversion () {
    uint initial_head_point = this->configuration->head_length + floor(this->random() * this->configuration->tail_length);
    uint final_head_point = this->configuration->head_length + floor(this->random() * this->configuration->tail_length);
    uint start = min(initial_head_point, final_head_point);
    uint end = max(initial_head_point, final_head_point) + 1;
    reverse(this->terminals.begin() + start, this->terminals.begin() + end);
    return true;
  }
};
#endif
