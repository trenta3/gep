#ifndef GEP_FUNCTIONS
#define GEP_FUNCTIONS
#include "terminals.hpp"
#include <math.h>

using namespace std;

static inline double _addition (vector<double> nums) { return nums[0] + nums[1]; }
constexpr Terminal<double, double> addt = make_terminal<double, double, _addition, 2>("+");

static inline double _multiplication (vector<double> nums) { return nums[0] * nums[1]; }
constexpr Terminal<double, double> mult = make_terminal<double, double, _multiplication, 2>("*");

static inline double _subtraction (vector<double> nums) { return nums[0] - nums[1]; }
constexpr Terminal<double, double> subt = make_terminal<double, double, _subtraction, 2>("-");

static inline double _division (vector<double> nums) { return nums[0] / nums[1]; }
constexpr Terminal<double, double> divt = make_terminal<double, double, _division, 2>("/");

static inline double _power (vector<double> nums) { return pow(nums[0], nums[1]); }
constexpr Terminal<double, double> powt = make_terminal<double, double, _power, 2>("^");

static inline double _sqrt (vector<double> nums) { return sqrt(nums[0]); }
constexpr Terminal<double, double> sqrtt = make_terminal<double, double, _sqrt, 1>("sqrt");
#endif
