#ifndef GEP_CONFIGURATION
#define GEP_CONFIGURATION
#include <vector>
#include "terminals.hpp"
#include "selection.hpp"
#include "utils.hpp"
#include "evolution.hpp"

using namespace std;

template <typename T, typename O>
class Configuration {
public:
  uint head_length;
  uint gene_number;
  uint max_arity;

  vector<Terminal<T, O>> function_terms;
  vector<Terminal<T, O>> attribute_terms;
  vector<Terminal<T, O>> all_terms;
  uint tail_length;
  uint total_length;

  linking_fptr<T> linking_function;

  EvolutionRates erates;

  Configuration (uint head_length, uint gene_number, vector<Terminal<T, O>> function_terms, vector<Terminal<T, O>> attribute_terms, linking_fptr<T> linking_function, EvolutionRates erates) {
    uint max_arity = 0;
    for (Terminal<T, O> ftm : function_terms)
      if (max_arity < ftm.arity)
	max_arity = ftm.arity;

    for (Terminal<T, O> ptm : attribute_terms)
      if (ptm.arity != 0)
	throw GEPException("An attribute term has arity different from zero!");

    this->head_length = head_length;
    this->gene_number = gene_number;

    this->function_terms = function_terms;
    this->attribute_terms = attribute_terms;
    this->all_terms.insert(this->all_terms.end(), function_terms.begin(), function_terms.end());
    this->all_terms.insert(this->all_terms.end(), attribute_terms.begin(), attribute_terms.end());
    this->linking_function = linking_function;
    this->max_arity = max_arity;
    
    this->tail_length = this->head_length * (this->max_arity - 1) + 1;
    this->total_length = this->tail_length + this->head_length;

    this->erates = erates;
  }
};
#endif
