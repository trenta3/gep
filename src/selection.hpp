#ifndef GEP_SELECTION
#define GEP_SELECTION
#include <vector>
#include <random>
#include "utils.hpp"

using namespace std;

template <typename T, typename O, class DerivedChromosome>
class Population;

template <typename T, typename O, class DerivedChromosome>
class SelectionStrategy {
public:
  virtual vector<DerivedChromosome> select (Population<T, O, DerivedChromosome> *population) {
    throw GEPException("Custom selection strategies need to override select.");
  }
};

template <class DerivedChromosome>
inline bool compare_chromosomes (DerivedChromosome& a, DerivedChromosome& b) {
  return a.fitness() > b.fitness();
}
#endif
