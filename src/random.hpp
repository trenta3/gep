#ifndef GEP_RANDOM
#define GEP_RANDOM
#include <random>

using namespace std;

static default_random_engine generator = default_random_engine(random_device()());
static uniform_real_distribution<> distribution(0.0, 1.0);

template <typename RandomGenerator = default_random_engine>
class standard_random {
public:
  inline double operator() () {
    return distribution(generator);
  }
};
#endif
